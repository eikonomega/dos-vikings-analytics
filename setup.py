from setuptools import setup, find_packages

import dos_vikings_analytics

setup(
    name="Dos Vikings Analytics",
    packages=find_packages(),
    version=dos_vikings_analytics.__version__,
    author="Mike Dunn",
    author_email="mike@eikonomega.com"
)