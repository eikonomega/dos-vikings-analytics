import os
import datetime
from functools import wraps

from flask import Flask, request, Response
from flask.ext.restful import Resource, Api, reqparse
from pymongo import MongoClient

app = Flask(__name__)
api = Api(app)

# Make an authenticated login to the database server.
mongodb = MongoClient(os.environ['MONGODB_CONNECTION_STRING'])

# Choose the application database from the server.
application_data = mongodb[os.environ['MONGODB_DATABASE_NAME']]

# Get the analytics_event collection.
analytics_event_collection = application_data['analytics_event']

# Get the authorized_users collection.
authorized_api_users = application_data['authorized_users']


def authenticate_or_fail(called_function):
    """
    Ensure that a request object has site_id and site_access_key in
    its payload or return a 401 status code.

    """
    @wraps(called_function)
    def authenticate(*args, **kwargs):
        if request.authorization:
            return called_function(*args, **kwargs)
        else:
            return (
                {"message": "You must provide a username/password per HTTP "
                            "Basic Auth to use this service."}, 401)

    return authenticate


class AnalyticsEvent(Resource):

    @authenticate_or_fail
    def get(self):
        """
        Obtain entries from the analytics_event collection of
        the program's MongoDB instance.

        Entries are filtered by matching request.authorization['username']
        to the 'site_id' field of the MongoDB documents.

        Returns:
            JSON payload containing requesting site's analytics events.

        """

        results = dict(events=dict())

        for event in analytics_event_collection.find(
                {"site_id": request.authorization['username']}):

            results['events'][event['_id'].__str__()] = dict(
                event_type=event['event_type'],
                event_message=event['event_message'],
                event_timestamp=event['event_timestamp'].__str__()
            )

        return results

    @authenticate_or_fail
    def post(self):
        """
        Create a new document in the analytics_event collection
        of the Mongo instance.

        The JSON payload of the request must include the following
        parameters:

            event_type (str): The 'type' of analytics event being recorded.
            event_message (str): Info to augment event_type.

        The function adds to additional attributes to the document
        data structure prior to saving:

            event_timestamp (timestamp)
            site_id (str): Set to the username of the authenticated
            user who initiated the HTTP Post call.

        Returns:
            Dict (converted to JSON by Flask) which contains the
            document_id of the new entry along with a success
            message.

        """
        request_parser = reqparse.RequestParser()

        request_parser.add_argument("event_type", type=str, required=True,
                                    help="An event type must be provided.")

        request_parser.add_argument("event_message", type=str, required=True,
                                    help="An event message must be provided.")

        post_data = request_parser.parse_args()

        post_data['event_timestamp'] = datetime.datetime.utcnow().isoformat()
        post_data['site_id'] = request.authorization['username']

        result = analytics_event_collection.insert(post_data)

        return dict(
            {"message": "Analytics Event Created",
             "record_id": result.__str__()}), 201

api.add_resource(AnalyticsEvent,
                 '/v1/events/<event_id>',
                 '/v1/events/')

if __name__ == '__main__':
    app.run(debug=True)



