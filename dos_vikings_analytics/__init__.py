"""
This library provides a simple Flask-based RESTful analytics API that
integrates with MongoDB backends.

Prerequisites
-------------

To use this application, you'll need the following:

1. A hosted MongoDB instance along with a database user/password with
   read & write privileges.
2. A web server capable of serving a Flask app.

Installation & Setup
--------------------

1.  Clone the Git repo: https://github.com/eikonomega/dos-vikings-analytics
    to your web server.

2.  Configure the following environment variables:

    - MONGODB_CONNECTION_STRING: mongodb://<username>:<password>@<host_server>:<server_port>/<your_database>
    - MONGODB_DATABASE_NAME: <your_database>

3.  Start the application up by running analytics_api.py.  You can do so
    easily by using the included Procfile.

Usage
-----
If you've made it this far, you should have a Flask app running on your
webserver (port 80 unless you've changed it) that's ready to provide an
RESTful API for storing and retrieving analytics data. **Yeah!**




"""

__version__ = "0.1.0"
