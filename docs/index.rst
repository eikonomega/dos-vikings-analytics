.. dos_vikings_analytics documentation master file, created by
   sphinx-quickstart on Wed Oct 23 00:59:19 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Dos Vikings Analytics
=================================================

.. automodule:: dos_vikings_analytics.__init__
    :members:
    :undoc-members:
    :show-inheritance:


.. toctree::
   :maxdepth: 2

   api_reference
