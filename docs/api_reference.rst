API Reference
=============================


AnalyticsEvent
---------------------------

.. module:: dos_vikings_analytics.analytics_api
.. autoclass:: AnalyticsEvent
    :members: get, post
    :undoc-members:
